import React from 'react';
import { StatusBar } from 'expo-status-bar';

import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Image
} from 'react-native';

export default function App() {
    return(
    <View style={styles.container}>
        <StatusBar style="auto" />
            <Image source={require('./image/logo.png')} style={styles.logo} />
            <Text style={styles.login}>Login</Text>
            <Text style={styles.usernameEmail}>Username / Email</Text>
            <TextInput style={styles.inputTextEmail}/>
            <Text style={styles.password}>Password</Text>
            <TextInput style={styles.inputTextPassword}/>
            
            <TouchableOpacity style={{ 
                position: 'absolute',
                width: 140,
                height: 40,
                left: 118,
                top: 493,
                backgroundColor: '#3EC6FF',
                borderRadius: 16, }}>
        </TouchableOpacity>
        <Text style={{ 
                position: 'absolute',
                width: 72,
                height: 28,
                left: 152,
                top: 499,

                fontFamily: 'Roboto',
                fontStyle: 'normal',
                fontWeight: 'normal',
                fontSize: 24,
                height: 28,
                color: 'white'
                }}>Masuk</Text>
            
            <Text style={styles.atau}>atau</Text>

            <TouchableOpacity style={{ 
                position: 'absolute',
                width: 140,
                height: 40,
                left: 118,
                top: 593,
                backgroundColor: '#003366',
                borderRadius: 16, }}>
        </TouchableOpacity>
        <Text style={{ 
                position: 'absolute',
                width: 84,
                height: 28,
                left: 146,
                top: 599,

                fontFamily: 'Roboto',
                fontStyle: 'normal',
                fontWeight: 'normal',
                fontSize: 24,
                height: 28,
                color: 'white'
                }}>Daftar?</Text>
        </View> 
    )
}

    const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#fff',
            alignItems: 'center',
            justifyContent: 'center',
        },

        logo:{
            position: 'absolute',
            width: 375,
            height: 102,
            left: 20,
            top: 63,
        },

        login:{
            position: 'absolute',
            width: 60,
            height: 28,
            left: 158,
            top: 235,

            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: 24,
            height: 28,
            color: '#003366'
        },

        usernameEmail:{
            position: 'absolute',
            width: 127,
            height: 19,
            left: 41,
            top: 303,

            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: 16,
            height: 19,
            color: '#003366'
        },

        inputTextEmail: {
            position: 'absolute',
            width: 294,
            height: 48,
            left: 41,
            top: 326,

            backgroundColor: '#FFFFFF',
            borderWidth: 1,
            borderColor: "#003366",
          },

          password:{
            position: 'absolute',
            width: 71,
            height: 19,
            left: 41,
            top: 390,

            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: 16,
            height: 19,
            color: '#003366'
          },

          inputTextPassword:{
            position: 'absolute',
            width: 294,
            height: 48,
            left: 41,
            top: 413,

            backgroundColor: '#FFFFFF',
            borderWidth: 1,
            borderColor: "#003366",  
          },

          atau:{
            position: 'absolute',
            width: 48,
            height: 28,
            left: 164,
            top: 549,

            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: 'normal',
            fontSize: 24,
            height: 28,
            color: '#3EC6FF'
          }
    });