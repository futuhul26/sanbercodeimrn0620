import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { FontAwesome } from '@expo/vector-icons'; 
import { FontAwesome5 } from '@expo/vector-icons'; 

import {
    StyleSheet,
    View,
    Text,
} from 'react-native';

export default function App() {
    return(
    <View style={styles.container}>
        <StatusBar style="auto" />
        <View style={styles.userDetailContainer}>
        <Text style={{fontSize: 36, color:'#003366'}}>Tentang Saya</Text>
            <FontAwesome5 name="user-secret" size={150} color="black" />
            <Text style={{fontSize: 28, fontWeight: 'bold', color: '#003366'}}>Saelemaekers</Text>
            <Text style={{fontSize: 18, color: '#3EC6FF', fontStyle: "italic"}}>React Native Developer</Text>
        </View>

        <View style={styles.portofolio}>
            <Text style={{fontSize: 18, color: '#003366'}}>Portofolio</Text>
            <View style={{marginTop: 4 , height: 0.5, backgroundColor: '#003366'}}/>
            <View style={styles.portofolioItemsLayout}>
              <View style={styles.portofolioItem}>
                <FontAwesome name="gitlab" size={50} color="#3EC6FF" />
                <Text style={{fontWeight: 'bold', color: '#003366'}}>Saelemaekers56</Text>
              </View>
              <View style={styles.portofolioItem}>
                <FontAwesome5 name="github" size={50} color="#3EC6FF" />
                <Text style={{fontWeight: 'bold', color: '#003366'}}>Saelemaekers56</Text>
              </View>
            </View>
          </View>

          <View style={styles.portofolio}>
            <Text style={{fontSize: 18, color: '#003366'}}>Hubungi Saya</Text>
            <View style={{marginTop: 4 , height: 0.5, backgroundColor: '#003366'}}/>
            <View style={styles.contactItemsLayout}>
              <View style={styles.contactItem}>
                <FontAwesome5 name="facebook" size={30} color="#3EC6FF" />
                <Text style={{fontWeight: 'bold', marginLeft: 20,color: '#003366'}}>Alexis Saelemaekers</Text>
              </View>
              <View style={styles.contactItem}>
                <FontAwesome5 name="instagram" size={30} color="#3EC6FF" />
                <Text style={{fontWeight: 'bold', marginLeft: 20, color: '#003366'}}>@Saelemaekers56</Text>
              </View>
              <View style={styles.contactItem}>
                <FontAwesome name="twitter" size={30} color="#3EC6FF" />
                <Text style={{fontWeight: 'bold', marginLeft: 20, color: '#003366'}}>@Saelemaekers56</Text>
              </View>
            </View>
          </View>

    </View> 
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    
    userDetailContainer: {
        marginTop: 45,
        alignItems: 'center',
        justifyContent: 'center',
      },

      portofolio: {
        margin: 15,
        borderRadius: 10,
        backgroundColor: '#EFEFEF',
        paddingHorizontal: 15,
      },
      portofolioItemsLayout: {
        padding: 15,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
      },
      portofolioItem: { 
        alignItems: 'center'
      },
      contactItemsLayout: {
        padding: 15,
        paddingHorizontal: 15,
        alignItems: 'center',
      },
      contactItem: {
        marginTop: 15,
        alignItems: 'center',
        flexDirection: 'row',
      }
  
  });