//soal A
console.log("Soal A")
function bandingkan (num1, num2){
    if(num1<0 || num2<0 || num1==num2 || num1==null || num2==null){
        return -1
    }
    else if(num1>num2){
        return num1
    }
    else{
        return num2
    }
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

//soal B
console.log('========================================')
console.log("Soal B")
function balikString(str){
    var kataLama = str;
    var kataBaru = '';
    for (let i = str.length - 1; i >= 0; i--) {
    kataBaru = kataBaru + kataLama[i];
     }
     return kataBaru;
    }
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah  

//Soal C
console.log('========================================')
console.log("Soal C")
function palindrome(str){
    for (var i = 0; i < str.length/2; i++) {
        if (str[i] !== str[str.length - 1 - i]) {
            return false;
        }
      }
      return true;
}

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
