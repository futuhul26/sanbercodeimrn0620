console.log("Soal A")
function DescendingTen(num){
    var a = ""
    for (i = num; i>num-10;i--){
        a = a + i + " ";
    }
    return a
}
console.log(DescendingTen(10)) //akan menampilkan 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen(20)) //akan menampilkan 20 19 18 17 16 15 14 13 12 11

console.log('========================================')
console.log("Soal B")
function AscendingTen(num){
    var a = ""
    for (i = num; i<num+10;i++){
        a = a + i + " ";
    }
    return a
}
console.log(AscendingTen(1)) //akan menampilkan 1 2 3 4 5 6 7 8 9 10  
console.log(AscendingTen(101)) //akan menampilkan 101 102 103 104 105 106 107 108 109 110

console.log('========================================')
console.log("Soal C")
function ConditionalAscDesc(reference, check){
    var a = ""
    if (check % 2==1){
        for (i = reference; i<reference+10;i++){
            a = a + i + " ";
        }
    }
    else{
        for (i = reference; i>reference-10;i--){
            a = a + i + " ";
        }
    }
    return a
}
console.log(ConditionalAscDesc(1, 1)) //akan menampilkan 1 2 3 4 5 6 7 8 9 10
console.log(ConditionalAscDesc(100, 4)) //akan menampilkan 100 99 98 97 96 95 94 93 92 91