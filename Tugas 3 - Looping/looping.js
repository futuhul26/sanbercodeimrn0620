//Soal No.1
console.log('No.1 Looping While==========================')
console.log('LOOPING PERTAMA')
var angka = 2;
var kalimat = ('I love coding')
while (angka <= 20 ){
    console.log(angka, ' - ', kalimat)
    angka += 2;
}

console.log('LOOPING KEDUA')

var angka2 = 20 ;
var kalimat2 = ('I will become a mobile developer')
while (angka2 >= 2 ){
    console.log(angka2, ' - ', kalimat2)
    angka2 -= 2;
}

//Soal No.2
console.log('No.2 Looping for==========================')
console.log('OUTPUT')
var angka3 = 1;
for (i = angka3; i<=20; i++){
    if (i%2 === 0){
        console.log(i + ' - Berkualitas');
    }
    else if(i%2==1 && i%3==0){
        console.log(i + ' - I Love Coding');
    }
    else{
        console.log(i + ' - Santai')
    }
}

//Soal No.3
console.log('No.3 Membuat Persegi Panjang==========================')
var x = 8
while(x<=32){
    if(x==8 || x==16 || x==24 || x==32){
        console.log("########");
    }
    x++;
}

//Soal No.4
console.log('No.4 Membuat Tangga==========================')
var x,y,char;
for(x=1; x<=8; x++){
    for (y=1; y<x; y++){
        char=char+("#");
    }
console.log(char);
char="";    
}

//Soal No.5
console.log('No.5 Membuat Papan Catur==========================')
var hash = "#";
var spasi = ' ';
var ukuran = 8;

for (var x = 1; x <= ukuran; x++) {
  var line = ' ';

  for (var y = 1; y <= ukuran; y++) {
    if (x % 2) {
      if (y % 2) {
        line = line + spasi;
      } else {
        line = line + hash;
      }
    } else {
      if (y % 2) {
        line = line + hash;
      } else {
        line = line + spasi;
      }
    }
  }
  console.log(line);
}