//Soal If Else
console.log("Soal If Else")
var nama = "Michael"
var peran = "Penyihir"

function IfElse(nama,peran) {
    if (nama == '') {
        console.log("Nama harus diisi!")
    } else if (nama && peran == '') {
        console.log("Halo " + nama + ", pilih peranmu untuk memulai game")
    } else if (nama == "Michael" && peran == "Penyihir"){
        console.log("Selamat datang di dunia Werewolf, Michael\nHalo Penyihir Michael, kamu dapat melihat siapa yang menjadi werewolf!")
    } else if (nama == "Trevor" && peran == "Guard"){
        console.log("Selamat datang di dunia Werewolf, Michael\nHalo Guard Trevor, kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }else if (nama == "Jimmy" && peran == "Werewolf"){
        console.log("Selamat datang di dunia Werewolf, Jimmy\nHalo Werewolf Jimmy, kamu akan memakan mangsa setiap malam!")
    }
}

IfElse('','')
IfElse('Franklin','')
IfElse('Michael','Penyihir')
IfElse('Trevor','Guard')
IfElse('Jimmy','Werewolf')

console.log("=============================================")

//Soal Switch Case
console.log("Soal Switch Case")
var tanggal = 26; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1999; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var teksBulan;

switch (true){
    case (tanggal < 1 || tanggal > 31 ):
        {
            console.log("Input tanggal salah")
            break;
        }
    case (bulan < 1 || bulan > 12):
        {
            console.log("Input bulan salah")
            break;
        }
    case (tahun < 1900 || tahun > 2200):
        {
            console.log("Input tahun salah")
            break;
        }
    default:
        {
            switch(bulan){
                case 1:
                    teksBulan = 'Januari';
                    break;
                case 2:
                    teksBulan = 'Februari';
                    break;
                case 3:
                    teksBulan = 'Maret';
                    break;
                case 4:
                    teksBulan = 'April';
                    break;
                case 5:
                    teksBulan = 'Mei';
                    break;
                case 6:
                    teksBulan = 'Juni';
                    break;
                case 7:
                    teksBulan = 'Juli';
                    break;
                case 8:
                    teksBulan = 'Agustus';
                    break;
                case 9:
                    teksBulan = 'September';
                    break;
                case 10:
                    teksBulan = 'Oktober';
                    break;
                case 11:
                    teksBulan = 'November';
                    break;
                case 12:
                    teksBulan = 'Desember';
                    break;
                default:
                    break;      
            }
            console.log(tanggal, '', teksBulan, '', tahun)
        }
    }    